from common.methods import set_progress
import os

def run(job, *args, **kwargs):

    username = "{{ username }}"
    ssh_key = "{{ ssh_key }}"
    
    script = "/mnt/nfs/sshkeys/sshdir.sh -u '{0}' -k '{1}'".format(username, ssh_key)
    
    os.system(script)